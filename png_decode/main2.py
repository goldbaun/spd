from libs.png_decode import decode, PNG_HEADER
from libs.fourier_transform import png_fft, check_png_fft

def main():
    with open('lots_of_chunks.png', 'rb') as file:
        chunks = decode(file)

        for chunk in chunks:
            print(chunk)

        mandatory_chunks = (chunk for chunk in chunks if chunk.chunk_name[0].isupper())
        with open('out.png', 'wb') as out_file:
            out_file.write(PNG_HEADER)
            for chunk in mandatory_chunks:
                print(f'writing chunk: {chunk}')
                out_file.write(chunk.to_bytes())

    png_fft('lots_of_chunks.png')
    check_png_fft('lots_of_chunks.png')


if __name__ == '__main__':
    main()